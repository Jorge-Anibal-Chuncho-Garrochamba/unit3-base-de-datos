import sqlite3
class Sistema_Registro:
    def __init__(self):
        self.conexion = sqlite3.connect('Registro.db')
        self.cursor = self.conexion.cursor()
        self.cursor.execute("""CREATE TABLE IF NOT EXISTS usuarios(
           idusuario INT PRIMARY KEY,
           nombre TEXT,
           email TEXT);
        """)
        self.conexion.commit()
        print("CONEXIÓN ESTABLECIDA")

    def Matricula_Estudiante(self):
        print("REGISTRO DE ESTUDIANTES:")
        id = str(input("ID ESTUDIANTE:\t"))
        nombre = str(input("NOMBRES:\t"))
        email = str(input("EMAIL:\t"))
        informacion = """INSERT INTO usuarios(idusuario, nombre, email) VALUES (?, ?,?) """
        datos = (id, nombre, email)
        self.cursor.execute(informacion, datos)
        self.conexion.commit()
        print("DATOS GUARDADOS")
        Volver = input("\nREGISTRAR OTRO ESTUDIANTE SI/NO:\t")
        if Volver == 'SI' or Volver == 'si':
            self.Matricula_Estudiante()
        elif Volver == 'NO' or Volver == 'no':
            self.menu()


    def Visualizar_Datos(self):
        print("USUARIOS REGISTRADOS")
        var = self.cursor.execute("""SELECT *  FROM usuarios""")
        for i in var:
            print ("ID =", i[0])
            print("NOMBRES =", i[1])
            print("EMAIL =", i[2])
            print("")

        print("DATOS CARGADOS COMPLETAMENTE")
        Volver = input("\nVOLVER AL MENU SI/NO:\t")
        if Volver == 'Si' or Volver == 'si':
            self.menu()
        elif Volver == 'NO' or Volver == 'no':
            self.conexion.close()
            print('CONEXION FINALIZADA')

    def Buscar_Estudiante(self):
        id = str(input("INGRESE ID USUARIO:\t"))
        self.cursor.execute("SELECT * FROM usuarios WHERE idusuario = (?)", (id,))
        var = self.cursor.fetchall()
        for i in var:
            print("ID = ", i[0])
            print("NOMBRE = ", i[1])
            print("EMAIL = ", i[2])
            Volver = input("\nBUSCAR NUEVAMENTE SI/NO:\t")
            if Volver == 'SI' or Volver == 'si':
                self.Buscar_Estudiante()
            elif Volver == 'NO' or Volver == 'no':
                self.menu()


    def Eliminar_Estudiante(self):
        print("ELIMINAR ALUMNO")
        id = str(input("ID DE ESTUDIANTE A ELIMINAR:\t"))
        self.cursor.execute("DELETE FROM usuarios WHERE idusuario = (?)", (id,))
        self.conexion.commit()
        print("USUARIO ELIMINADO")
        Volver = input("\nELIMINAR OTRO ESTUDIANTE SI/NO:\t")
        if Volver == 'SI' or Volver == 'si':
            self.Eliminar_Estudiante()
        elif Volver == 'NO' or Volver == 'no':
            self.menu()

    def menu(self):
        print("SISTEMA DE GESTION ESTUDIANTIL")
        print("""
            1) Matricular Estudiante    3) Buscar Estudiante
            2) Visualizar Datos         4) Eliminar Estuiante  
            0) Salir \n """)

        selec = input("INGRESE ACCION:\t")

        if selec == "1":
            self.Matricula_Estudiante()
        elif selec == "2":
            self.Visualizar_Datos()
        elif selec == "3":
            self.Buscar_Estudiante()
        elif selec == "4":
            self.Eliminar_Estudiante()
        elif selec == "0":
            print("CONEXION TERMINADA")
            self.conexion.close()

        else:
            print("PROGRAMA TERMINADO")
            self.conexion.close()
            print("CONEXION TERMINADA")

if __name__ == "__main__":
    inicio =Sistema_Registro()
    inicio.menu()
